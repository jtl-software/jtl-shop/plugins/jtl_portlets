<label for="config-{$propname}">
    {$propdesc.label}
</label>
<input type="range" id="config-{$propname}" name="{$propname}" value="{$propval}" class="form-control">
<script>
    opc.once('process-portlet-property:{$propname}', data => {
        data.value = parseFloat(data.value);
    });
</script>