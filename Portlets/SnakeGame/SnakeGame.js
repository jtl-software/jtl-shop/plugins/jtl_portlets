(function () {
    const snakeGames = document.querySelectorAll('canvas.jtl_portlets_SnakeGame');

    for(const snakeGame of snakeGames) {
        initSnakeGame(snakeGame);
    }

    function initSnakeGame(canvas)
    {
        const snakeColor = canvas.dataset.snakeColor;
        const appleColor = canvas.dataset.appleColor;
        const backgroundColor = canvas.dataset.backgroundColor;
        const ctx = canvas.getContext('2d');

        let snake = [[8,8]];
        let apple = [4,4];
        let dir = [0,0];

        canvas.addEventListener('keydown', e => {
            switch(e.key) {
                case 'ArrowLeft':
                    if(dir[0] === 0) {
                        dir = [-1,0];
                    }
                    e.preventDefault();
                    break;
                case 'ArrowRight':
                    if(dir[0] === 0) {
                        dir = [+1,0];
                    }
                    e.preventDefault();
                    break;
                case 'ArrowUp':
                    if(dir[1] === 0) {
                        dir = [0,-1];
                    }
                    e.preventDefault();
                    break;
                case 'ArrowDown':
                    if(dir[1] === 0) {
                        dir = [0,+1];
                    }
                    e.preventDefault();
                    break;
            }
        });

        window.setInterval(() => {
            let new_head = [
                (snake[0][0] + dir[0] + 16) % 16,
                (snake[0][1] + dir[1] + 16) % 16,
            ];

            snake.unshift(new_head);

            if(snake[0] + '' === apple + '') {
                do {
                    apple = [
                        Math.floor(Math.random() * 16),
                        Math.floor(Math.random() * 16),
                    ];
                } while (snake.some(segment => segment + '' === apple + ''));
            } else if(snake.slice(1).some(segment => segment + '' === snake[0] + '')) {
                snake.splice(1);
            } else {
                snake.pop();
            }

            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = appleColor;
            ctx.fillRect(apple[0] * 16, apple[1] * 16, 16, 16);
            ctx.fillStyle = snakeColor;

            for(const [x,y] of snake) {
                ctx.fillRect(x * 16, y * 16, 16, 16);
            }
        }, 125);
    }
})();