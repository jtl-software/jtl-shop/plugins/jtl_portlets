<?php declare(strict_types=1);

namespace Plugin\jtl_portlets\Portlets\SnakeGame;

use JTL\OPC\InputType;
use JTL\OPC\Portlet;

class SnakeGame extends Portlet
{
    public function getPropertyDesc(): array
    {
        return [
            'snake-color' => [
                'type' => InputType::COLOR,
                'label' => __('Snake color'),
                'default' => '#0f0',
                'width' => 33,
            ],
            'apple-color' => [
                'type' => InputType::COLOR,
                'label' => __('Apple color'),
                'default' => '#f00',
                'width' => 33,
            ],
            'background-color' => [
                'type' => InputType::COLOR,
                'label' => __('Background color'),
                'default' => '#000',
                'width' => 33,
            ],
        ];
    }

    public function getExtraJsFiles(): array
    {
        return [
            $this->plugin->getPaths()->getPortletsUrl() . 'SnakeGame/SnakeGame.js'
        ];
    }
}