{if $isPreview}
    <img src="{$portlet->getPlugin()->getPaths()->getPortletsUrl()}SnakeGame/icon.svg" width="256" height="256">
{else}
<canvas width="256" height="256" class="jtl_portlets_SnakeGame" tabindex="-1"
        data-snake-color="{$instance->getProperty('snake-color')}"
        data-apple-color="{$instance->getProperty('apple-color')}"
        data-background-color="{$instance->getProperty('background-color')}"></canvas>
{/if}