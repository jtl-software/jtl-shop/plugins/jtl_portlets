<div class="alert alert-{$instance->getProperty('type-select')} {$instance->getAnimationClass()} {$instance->getStyleClasses()}"
     style="{$instance->getStyleString()}">
    <i class="{$instance->getProperty('x4')}" style="color:{$instance->getProperty('x2')}"></i>
    {$instance->getProperty('some-text')|nl2br}
</div>