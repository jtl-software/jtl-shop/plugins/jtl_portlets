<?php declare(strict_types=1);

namespace Plugin\jtl_portlets\Portlets\Alert;

use JTL\OPC\InputType;
use JTL\OPC\Portlet;
use JTL\Shop;

/**
 * Class Alert
 * @package Plugin\jtl_portlets\Portlets
 */
class Alert extends Portlet
{
    /**
     * @return string
     */
    public function getButtonHtml(): string
    {
        return $this->getFontAwesomeButtonHtml('fas fa-exclamation-circle');
    }

    /**
     * @return array
     */
    public function getPropertyDesc(): array
    {
        return [
            'some-text'   => [
                'label'   => \__('a text'),
                'type'    => InputType::TEXTAREA,
                'default' => \__('Hello world!'),
            ],
            'type-select' => [
                'label'   => \__('Alert Type'),
                'type'    => InputType::SELECT,
                'options' => [
                    'success' => \__('Success'),
                    'info'    => \__('Info'),
                    'warning' => \__('Warning'),
                    'danger'  => \__('Danger'),
                ],
                'default' => 'info',
            ],
            'somefoo'     => [
                'label'   => \__('a slider property'),
                'type'    => 'jtl_portlets.range-slider',
            ],
            'x2'          => [
                'label'   => \__('Color'),
                'type'    => InputType::COLOR,
                'default' => '#ffffff',
            ],
            'x3'          => [
                'label'   => \__('A date'),
                'type'    => InputType::DATE,
                'default' => null,
            ],
            'x4'          => [
                'label'   => \__('Icon'),
                'type'    => InputType::ICON,
                'default' => null,
            ],
            'x5'          => [
                'label'   => \__('Image set'),
                'type'    => InputType::IMAGE_SET,
                'default' => null,
            ],
            'x6'          => [
                'label'   => \__('Image'),
                'type'    => InputType::IMAGE,
                'default' => null,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getPropertyTabs(): array
    {
        return [
            \__('Styles')    => 'styles',
            \__('Animation') => 'animations',
        ];
    }
}
